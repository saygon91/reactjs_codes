/* eslint-disable quote-props */

export const CATEGORIES_NAMES = {
  prodazha: 'Запчасти',
  shiny: 'Шины',
  diski: 'Диски',
  aksessuary: 'Акссессуары и мультимедиа',
  consumables: 'Расходники',
  carsforparts: 'Машина на запчасти',
  services: 'Услуги',
  drugie: 'Другие',
};

export const FIELD_SCHEMES = {
  prodazha: {},
  shiny: {},
  diski: {},
  aksessuary: {},
  consumables: {},
  carsforparts: {
    'Кузов': { type: 'text', name: 'Кузов', placeholder: 'седан' },
    'Привод': { type: 'text', name: 'Привод', placeholder: 'передний привод' },
    'Объем двигателя, л': { type: 'text', name: 'Объем двигателя, л', placeholder: '1.6 (бензин)' },
    'Растаможен': { type: 'text', name: 'Растаможен', placeholder: 'да' },
    'Руль': { type: 'text', name: 'Руль', placeholder: 'слева' },
    'Коробка передач': { type: 'text', name: 'Коробка передач', placeholder: 'механика' },
    'Пробег': { type: 'text', name: 'Пробег', placeholder: '10 000 км' },
    'Цвет': { type: 'text', name: 'Цвет', placeholder: 'синий металлик' },
  },
  services: {},
  drugie: {},
};

/* eslint-enable quote-props */
