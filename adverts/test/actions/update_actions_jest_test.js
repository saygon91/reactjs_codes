import { call, put } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import { updateAdvertsRequest, updateAdverts } from 'client/adverts/actions/update_actions.js';

const id = 1;
const errorStatus = 401;
const errors = ['401 NOT AUTHORIZED'];
const params = { id, advert: { content: 'Jesus is alive' }, data: { empty: false } };
const advert = { id, title: 'Cook & Meal' };

describe('#updateAdvertsRequest', () => {
  const expected = { type: ActionTypes.UPDATE_ADVERTS_REQUEST, params };
  const actual = updateAdvertsRequest(params);

  it('is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#updateAdverts success', () => {
  let actual;
  let expected;
  const generator = updateAdverts(params);

  actual = generator.next().value;
  expected = call(API.updateAdverts, params);
  it('Update advert through API is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({ data: { obv: advert } }).value;
  expected = put({ type: ActionTypes.UPDATE_ADVERTS_SUCCESS, advert });
  it('Dispatch success action is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next().value;
  expected = call(browserHistory.push, `/obvs/${advert.id}`);
  it('Redirect to advert is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#updateAdverts failure', () => {
  let actual;
  let expected;
  const generator = updateAdverts(params);

  actual = generator.next().value;
  expected = call(API.updateAdverts, params);
  it('Update advert through API is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({ status: errorStatus, data: { errors } }).value;
  expected = put({ type: ActionTypes.UPDATE_ADVERTS_FAILURE, errors });
  it('Dispatch failure action is ok', () => {
    expect(expected).toEqual(actual);
  });
});
