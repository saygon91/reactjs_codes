import { call, put } from 'redux-saga/effects';

import { Advert } from 'test/factories.js';
import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import { searchAdvertsRequest, searchAdverts } from 'client/adverts/actions/search_actions.js';

const errorStatus = 401;
const errors = ['401 NOT AUTHORIZED'];
const advert = Advert.build();
const adverts = { [advert.id]: advert };
const params = { text: 'text' };
const meta = { next_page: 1 };
const reset = false;

describe('#searchAdvertsRequest', () => {
  it('is ok', () => {
    const expected = { type: ActionTypes.SEARCH_ADVERTS_REQUEST, params, reset };
    const actual = searchAdvertsRequest(params, reset);

    expect(expected).toEqual(actual);
  });
});

describe('#searchAdverts success', () => {
  const generator = searchAdverts(params, reset);

  it('Search adverts through API', () => {
    const actual = generator.next().value;
    const expected = call(API.searchAdverts, params);

    expect(expected).toEqual(actual);
  });

  it('Dispatch success actionk', () => {
    const actual = generator.next({ data: { obvs: adverts, meta } }).value;
    const expected = put({ type: ActionTypes.SEARCH_ADVERTS_SUCCESS, reset, adverts, meta });

    expect(expected).toEqual(actual);
  });
});

describe('#searchAdverts failure', () => {
  const generator = searchAdverts(params, reset);

  it('Search adverts through API', () => {
    const actual = generator.next().value;
    const expected = call(API.searchAdverts, params);

    expect(expected).toEqual(actual);
  });

  it('Dispatch failure action', () => {
    const actual = generator.next({ status: errorStatus, data: { errors } }).value;
    const expected = put({ type: ActionTypes.SEARCH_ADVERTS_FAILURE, errors });

    expect(expected).toEqual(actual);
  });
});
