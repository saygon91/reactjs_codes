import { call, put } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

import { stubBrowserHistory } from 'test/stubs.js';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import { deleteAdvertsRequest, deleteAdverts } from 'client/adverts/actions/delete_actions.js';

const id = 1;
const errorStatus = 401;
const errors = ['401 NOT AUTHORIZED'];

describe('#deleteAdvertsRequest', () => {
  const expected = { type: ActionTypes.DELETE_ADVERTS_REQUEST, id };
  const actual = deleteAdvertsRequest(id);

  it('is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#deleteAdverts success', () => {
  let actual;
  let expected;
  const generator = deleteAdverts(id);

  actual = generator.next().value;
  expected = call(API.deleteAdverts, id);
  it('Delete advert through API is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({}).value;
  expected = put({ type: ActionTypes.DELETE_ADVERTS_SUCCESS, advert_id: id });
  it('Dispatch success action is ok', () => {
    expect(expected).toEqual(actual);
  });

  stubBrowserHistory();
  actual = generator.next().value;
  expected = call(browserHistory.push, '/obvs/own');
  it('Redirect to own adverts index page is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#deleteAdverts failure', () => {
  let actual;
  let expected;
  const generator = deleteAdverts(id);

  actual = generator.next().value;
  expected = call(API.deleteAdverts, id);
  it('Delete advert through API is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({ status: errorStatus, data: { errors } }).value;
  expected = put({ type: ActionTypes.DELETE_ADVERTS_FAILURE, errors });
  it('Dispatch failure action is ok', () => {
    expect(expected).toEqual(actual);
  });
});
