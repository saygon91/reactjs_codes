import { call, put } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

import { stubBrowserHistory } from 'test/stubs.js';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import { createAdvertsRequest, createAdverts } from 'client/adverts/actions/create_actions.js';

const id = 1;
const errorStatus = 401;
const errors = ['401 NOT AUTHORIZED'];
const params = { title: 'Cook & Meal' };
const advert = { id: 1, title: 'Cook & Meal' };

describe('#createAdvertsRequest', () => {
  const expected = { type: ActionTypes.CREATE_ADVERTS_REQUEST, params };
  const actual = createAdvertsRequest(params);

  it('is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#createAdvert success', () => {
  let actual;
  let expected;
  const generator = createAdverts(params);

  actual = generator.next().value;
  expected = call(API.createAdverts, params);
  it('Create auto note through API is ok', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({ data: { obv: advert } }).value;
  expected = put({ type: ActionTypes.CREATE_ADVERTS_SUCCESS, advert });
  it('Dispatch success action is ok', () => {
    expect(expected).toEqual(actual);
  });

  stubBrowserHistory();
  actual = generator.next({ data: { obv: advert } }).value;
  expected = call(browserHistory.push, '/obvs/own');
  it('Redirect to auto note is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('#createAdvert failure', () => {
  let actual;
  let expected;
  const generator = createAdverts(id);

  actual = generator.next().value;
  expected = call(API.createAdverts, id);
  it('Create auto note through API', () => {
    expect(expected).toEqual(actual);
  });

  actual = generator.next({ status: errorStatus, data: { errors } }).value;
  expected = put({ type: ActionTypes.CREATE_ADVERTS_FAILURE, errors });
  it('Dispatch failure action', () => {
    expect(expected).toEqual(actual);
  });
});
