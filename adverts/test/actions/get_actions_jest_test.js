import { call, put } from 'redux-saga/effects';

import { Advert } from 'test/factories.js';
import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import { getAdvertRequest, getAdvert, getAdvertsRequest, getAdverts } from 'client/adverts/actions/get_actions.js';

const errorStatus = 401;
const errors = ['401 NOT AUTHORIZED'];
const advert = Advert.build();
const { id } = advert;
const params = { id, advert };

describe('#getAdvertRequest', () => {
  it('is ok', () => {
    const expected = { type: ActionTypes.GET_ADVERT_REQUEST, id };
    const actual = getAdvertRequest(id);

    expect(expected).toEqual(actual);
  });
});

describe('#getAdvert success', () => {
  const generator = getAdvert(id);

  it('Fetch advert through API', () => {
    const actual = generator.next().value;
    const expected = call(API.getAdvert, id);

    expect(expected).toEqual(actual);
  });

  it('Dispatch success action', () => {
    const actual = generator.next({ data: { obv: advert } }).value;
    const expected = put({ type: ActionTypes.GET_ADVERT_SUCCESS, advert });

    expect(expected).toEqual(actual);
  });
});

describe('#getAdvert failure', () => {
  const generator = getAdvert(id);

  it('Fetch advert through API', () => {
    const actual = generator.next().value;
    const expected = call(API.getAdvert, id);

    expect(expected).toEqual(actual);
  });

  it('Dispatch failure action', () => {
    const actual = generator.next({ status: errorStatus, data: { errors } }).value;
    const expected = put({ type: ActionTypes.GET_ADVERT_FAILURE, errors });

    expect(expected).toEqual(actual);
  });
});

describe('#getAdvertsRequest', () => {
  it('is ok', () => {
    const expected = { type: ActionTypes.GET_ADVERTS_REQUEST, params };
    const actual = getAdvertsRequest(params);

    expect(expected).toEqual(actual);
  });
});

describe('#getAdverts success', () => {
  const generator = getAdverts(id);

  it('Fetch adverts through API', () => {
    const actual = generator.next().value;
    const expected = call(API.getAdverts, id);

    expect(expected).toEqual(actual);
  });

  it('Dispatch success action', () => {
    const actual = generator.next({ data: { obvs: [advert] } }).value;
    const expected = put({ type: ActionTypes.GET_ADVERTS_SUCCESS, adverts: [advert] });

    expect(expected).toEqual(actual);
  });
});

describe('#getAdverts failure', () => {
  const generator = getAdverts(id);

  it('Fetch adverts through API', () => {
    const actual = generator.next().value;
    const expected = call(API.getAdverts, id);

    expect(expected).toEqual(actual);
  });

  it('Dispatch failure action', () => {
    const actual = generator.next({ status: errorStatus, data: { errors } }).value;
    const expected = put({ type: ActionTypes.GET_ADVERTS_FAILURE, errors });

    expect(expected).toEqual(actual);
  });
});
