import React from 'react';
import renderer from 'react-test-renderer';

import { Advert } from 'test/factories.js';

import Item from 'client/adverts/components/_details_form.jsx';

const advert = Advert.build();

const props = {
  category: advert.category,
  details: advert.details,
  onChange: jest.fn(),
};

it('renders correctly', () => {
  const tree = renderer.create(
    <Item {...props} />
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

it('renders with empty details', () => {
  const temp_props = { ...props, details: {} };

  const tree = renderer.create(
    <Item {...temp_props} />
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
