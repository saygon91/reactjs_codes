import reducer from 'client/adverts/reducer.js';
import { Advert } from 'test/factories.js';

import { ActionTypes } from 'client/constants.js';

const DEFAULT_STATE = {
  is_processing: false,
  is_searching: false,
  errors: [],
  adverts: {},
  own_adverts: {},
  search_adverts: {},
  current_advert_id: null,
  meta: { next_page: 1 },
};
const advert = Advert.build();
const adverts = { [advert.id]: advert };
const errors = ['403 UNAUTHORISED USER'];

describe('Default state', () => {
  it('is ok', () => {
    const actual = reducer(undefined, { type: 'UNDEFINED_TYPE' });
    expect(DEFAULT_STATE).toEqual(actual);
  });
});

describe('Requests', () => {
  const expected = { ...DEFAULT_STATE, is_processing: true };
  let actual;

  it('get advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.GET_ADVERT_REQUEST });
    expect(expected).toEqual(actual);
  });

  it('get adverts is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.GET_ADVERTS_REQUEST });
    expect(expected).toEqual(actual);
  });

  it('create advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.CREATE_ADVERTS_REQUEST });
    expect(expected).toEqual(actual);
  });

  it('update advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.UPDATE_ADVERTS_REQUEST });
    expect(expected).toEqual(actual);
  });

  it('delete advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.DELETE_ADVERTS_REQUEST });
    expect(expected).toEqual(actual);
  });
});

describe('Search adverts request', () => {
  const expected = { ...DEFAULT_STATE, is_searching: true };
  const actual = reducer(undefined, { type: ActionTypes.SEARCH_ADVERTS_REQUEST });

  it('is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('Failures', () => {
  const expected = { ...DEFAULT_STATE,
    is_processing: false,
    errors,
  };
  let actual;

  it('get advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.GET_ADVERT_FAILURE, errors });
    expect(expected).toEqual(actual);
  });

  it('get adverts is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.GET_ADVERTS_FAILURE, errors });
    expect(expected).toEqual(actual);
  });

  it('create advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.CREATE_ADVERTS_FAILURE, errors });
    expect(expected).toEqual(actual);
  });

  it('update advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.UPDATE_ADVERTS_FAILURE, errors });
    expect(expected).toEqual(actual);
  });

  it('delete advert is ok', () => {
    actual = reducer(undefined, { type: ActionTypes.DELETE_ADVERTS_FAILURE, errors });
    expect(expected).toEqual(actual);
  });
});

describe('Search adverts failure', () => {
  const expected = { ...DEFAULT_STATE, is_searching: false };
  const actual = reducer(undefined, { type: ActionTypes.SEARCH_ADVERTS_FAILURE, errors });

  it('is ok', () => {
    expect(expected).toEqual(actual);
  });
});

describe('Get adverts success', () => {
  const beforeState = { ...DEFAULT_STATE, is_processing: true };

  it('is ok', () => {
    const expected = { ...DEFAULT_STATE, own_adverts: { [advert.id]: advert } };
    const actual = reducer(beforeState, { type: ActionTypes.GET_ADVERTS_SUCCESS, adverts: [advert], meta: { next_page: 1 } });

    expect(expected).toEqual(actual);
  });
});

describe('Get, create, update advedrt', () => {
  const beforeState = { ...DEFAULT_STATE, is_processing: true };
  const expected = { ...DEFAULT_STATE, adverts, current_advert_id: advert.id };
  let actual;

  it('get advert is ok', () => {
    actual = reducer(beforeState, { type: ActionTypes.GET_ADVERT_SUCCESS, advert });
    expect(expected).toEqual(actual);
  });

  it('create advert is ok', () => {
    actual = reducer(beforeState, { type: ActionTypes.CREATE_ADVERTS_SUCCESS, advert });
    expect(expected).toEqual(actual);
  });

  it('update advert is ok', () => {
    actual = reducer(beforeState, { type: ActionTypes.UPDATE_ADVERTS_SUCCESS, advert });
    expect(expected).toEqual(actual);
  });
});

describe('Delete advedrt success', () => {
  const beforeState = { ...DEFAULT_STATE, adverts, is_processing: true };
  const actual = reducer(beforeState, { type: ActionTypes.DELETE_ADVERTS_SUCCESS, advert_id: advert.id });

  it('is ok', () => {
    expect(DEFAULT_STATE).toEqual(actual);
  });
});
