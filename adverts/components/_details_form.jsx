import React, { Component, PropTypes } from 'react';

import { FIELD_SCHEMES } from 'client/adverts/constants.js';

class DetailsForm extends Component {
  constructor() {
    super();

    this._handleInputChange = this._handleInputChange.bind(this);
  }

  componentWillMount() {
    const { details, category } = this.props;

    const empty_fields = R.map(() => '', FIELD_SCHEMES[category]);

    this.setState({ ...empty_fields, ...details });
  }

  componentDidUpdate(prevProps, prevState) {
    const { onChange } = this.props;

    if (!R.equals(this.state, prevState) && onChange) onChange(this.state);
  }

  _handleInputChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const { category } = this.props;

    return (
      R.keys(FIELD_SCHEMES[category]).length ?
        <column data-padding="16px">
          <row className="-items-center" data-padding="8px" cellSpacing="8px">
            <div className="h6">Дополнительные поля</div>
            <splitter />
          </row>
          <row data-children-width="50% md:1/3" cellPadding="4px" data-padding="8px">
            {
              R.keys(FIELD_SCHEMES[category]).map(field =>
                <column key={field} cellSpacing="4px">
                  <label htmlFor={field}>{FIELD_SCHEMES[category][field].name}</label>
                  <mg-input>
                    <input
                      className="input"
                      name={field}
                      type={FIELD_SCHEMES[category][field].type}
                      placeholder={FIELD_SCHEMES[category][field].placeholder}
                      onChange={this._handleInputChange}
                      value={this.state[field]}
                    />
                  </mg-input>
                </column>
              )
            }
          </row>
        </column>
      : null
    );
  }
}

DetailsForm.propTypes = {
  category: PropTypes.string.isRequired,
  details: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

export default DetailsForm;
