import { browserHistory } from 'react-router';
import { call, put, take, fork } from 'redux-saga/effects';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import errors from 'shared/utils/errors_utils.js';

export const updateAdvertsRequest = (params) => {
  return { type: ActionTypes.UPDATE_ADVERTS_REQUEST, params };
};

export function* updateAdverts(params) {
  const res = yield call(API.updateAdverts, params);

  if (res.status > 400) {
    yield put({ type: ActionTypes.UPDATE_ADVERTS_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.UPDATE_ADVERTS_SUCCESS, advert: res.data.obv });

    yield call(browserHistory.push, `/obvs/${params.id}`);
  }
}

export function* watchUpdateAdverts() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { params } = yield take(ActionTypes.UPDATE_ADVERTS_REQUEST);

    yield fork(updateAdverts, params);
  }
}
