import { call, put, take, fork } from 'redux-saga/effects';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import errors from 'shared/utils/errors_utils.js';

export const getAdvertsRequest = (params) => {
  return { type: ActionTypes.GET_ADVERTS_REQUEST, params };
};

export const getAdvertRequest = (id) => {
  return { type: ActionTypes.GET_ADVERT_REQUEST, id };
};

export function* getAdverts(params) {
  const res = yield call(API.getAdverts, params);

  if (res.status > 400) {
    yield put({ type: ActionTypes.GET_ADVERTS_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.GET_ADVERTS_SUCCESS, adverts: res.data.obvs, meta: res.data.meta });
  }
}

export function* watchGetAdverts() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { params } = yield take(ActionTypes.GET_ADVERTS_REQUEST);

    yield fork(getAdverts, params);
  }
}

export function* getAdvert(id) {
  const res = yield call(API.getAdvert, id);

  if (res.status > 400) {
    yield put({ type: ActionTypes.GET_ADVERT_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.GET_ADVERT_SUCCESS, advert: res.data.obv });
  }
}

export function* watchGetAdvert() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { id } = yield take(ActionTypes.GET_ADVERT_REQUEST);

    yield fork(getAdvert, id);
  }
}
