import { call, put, take, fork } from 'redux-saga/effects';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import errors from 'shared/utils/errors_utils.js';

export const searchAdvertsRequest = (params, reset) => {
  return { type: ActionTypes.SEARCH_ADVERTS_REQUEST, params, reset };
};

export function* searchAdverts(params, reset) {
  const res = yield call(API.searchAdverts, params);

  if (res.status > 400) {
    yield put({ type: ActionTypes.SEARCH_ADVERTS_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.SEARCH_ADVERTS_SUCCESS, reset, adverts: res.data.obvs, meta: res.data.meta });
  }
}

export function* watchSearchAdverts() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { params, reset } = yield take(ActionTypes.SEARCH_ADVERTS_REQUEST);

    yield fork(searchAdverts, params, reset);
  }
}
