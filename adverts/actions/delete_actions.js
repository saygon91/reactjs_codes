import { call, put, take, fork } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import errors from 'shared/utils/errors_utils.js';

export const deleteAdvertsRequest = (id) => {
  return { type: ActionTypes.DELETE_ADVERTS_REQUEST, id };
};

export function* deleteAdverts(id) {
  const res = yield call(API.deleteAdverts, id);

  if (res.status > 400) {
    yield put({ type: ActionTypes.DELETE_ADVERTS_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.DELETE_ADVERTS_SUCCESS, advert_id: id });

    yield call(browserHistory.push, '/obvs/own');
  }
}

export function* watchDeleteAdverts() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { id } = yield take(ActionTypes.DELETE_ADVERTS_REQUEST);

    yield fork(deleteAdverts, id);
  }
}
