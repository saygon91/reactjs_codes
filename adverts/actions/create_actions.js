import { browserHistory } from 'react-router';
import { call, put, take, fork } from 'redux-saga/effects';

import { ActionTypes } from 'client/constants.js';
import API from 'client/web_api_utils.js';
import errors from 'shared/utils/errors_utils.js';

export const createAdvertsRequest = (params) => {
  return { type: ActionTypes.CREATE_ADVERTS_REQUEST, params };
};

export function* createAdverts(params) {
  const res = yield call(API.createAdverts, params);

  if (res.status > 400) {
    yield put({ type: ActionTypes.CREATE_ADVERTS_FAILURE, errors: errors(res) });
  } else {
    yield put({ type: ActionTypes.CREATE_ADVERTS_SUCCESS, advert: res.data.obv });

    yield call(browserHistory.push, '/obvs/own');
  }
}

export function* watchCreateAdverts() {
  while (ActionTypes.TRUE_CONSTANT) {
    const { params } = yield take(ActionTypes.CREATE_ADVERTS_REQUEST);

    yield fork(createAdverts, params);
  }
}
