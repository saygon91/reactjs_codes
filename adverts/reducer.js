import { ActionTypes } from 'client/constants.js';

function _getDefaultState() {
  return {
    is_processing: false,
    is_searching: false,
    errors: [],
    adverts: {},
    own_adverts: {},
    search_adverts: {},
    current_advert_id: null,
    meta: { next_page: 1 },
  };
}

function advert_state(state = _getDefaultState(), action) {
  switch (action.type) {
    case ActionTypes.GET_ADVERT_REQUEST:
    case ActionTypes.GET_ADVERTS_REQUEST:
    case ActionTypes.CREATE_ADVERTS_REQUEST:
    case ActionTypes.UPDATE_ADVERTS_REQUEST:
    case ActionTypes.DELETE_ADVERTS_REQUEST:
      return { ...state, errors: [], is_processing: true };

    case ActionTypes.GET_ADVERT_FAILURE:
    case ActionTypes.GET_ADVERTS_FAILURE:
    case ActionTypes.CREATE_ADVERTS_FAILURE:
    case ActionTypes.UPDATE_ADVERTS_FAILURE:
    case ActionTypes.DELETE_ADVERTS_FAILURE:
      return { ...state, errors: action.errors, is_processing: false };

    case ActionTypes.GET_ADVERTS_SUCCESS:
      return { ...state,
        errors: [],
        is_processing: false,
        own_adverts: {
          ...R.indexBy(R.prop('id'), action.adverts), ...state.own_adverts,
        },
        meta: action.meta,
      };
    case ActionTypes.GET_ADVERT_SUCCESS:
    case ActionTypes.CREATE_ADVERTS_SUCCESS:
    case ActionTypes.UPDATE_ADVERTS_SUCCESS:
      return Object.assign({}, state, {
        errors: [],
        is_processing: false,
        adverts: Object.assign({}, state.adverts, { [action.advert.id]: action.advert }),
        current_advert_id: action.advert.id,
      });
    case ActionTypes.DELETE_ADVERTS_SUCCESS:
      return Object.assign({}, state, {
        errors: [],
        is_processing: false,
        adverts: R.omit(`${action.advert_id}`, state.adverts),
      });

    case ActionTypes.SEARCH_ADVERTS_REQUEST:
      return { ...state, is_searching: true };
    case ActionTypes.SEARCH_ADVERTS_FAILURE:
      return { ...state, is_searching: false };
    case ActionTypes.SEARCH_ADVERTS_SUCCESS:
      return { ...state,
        errors: [],
        is_searching: false,
        search_adverts: {
          ...(action.reset ? {} : state.search_adverts),
          ...R.indexBy(R.prop('id'), action.adverts),
        },
        meta: action.meta,
      };
    default:
      return state;
  }
}

export default advert_state;
