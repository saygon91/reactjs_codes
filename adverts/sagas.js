import { watchGetAdvert, watchGetAdverts } from 'client/adverts/actions/get_actions.js';
import { watchSearchAdverts } from 'client/adverts/actions/search_actions.js';
import { watchDeleteAdverts } from 'client/adverts/actions/delete_actions.js';
import { watchCreateAdverts } from 'client/adverts/actions/create_actions.js';
import { watchUpdateAdverts } from 'client/adverts/actions/update_actions.js';

export default [
  watchGetAdvert,
  watchGetAdverts,
  watchSearchAdverts,
  watchDeleteAdverts,
  watchCreateAdverts,
  watchUpdateAdverts,
];
