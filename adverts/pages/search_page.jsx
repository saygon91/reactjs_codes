import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { debounce } from 'throttle-debounce';
import Waypoint from 'react-waypoint';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

import BaseHoc from 'client/shared/base_hoc.jsx';
import Select from 'shared/components/select.jsx';
import Loader from 'shared/components/spinner.jsx';
import LinkButton from 'shared/components/link_button.jsx';
import Ability from 'shared/components/ability.jsx';

import { CATEGORIES_NAMES } from 'client/adverts/constants.js';

import { searchAdvertsRequest } from 'client/adverts/actions/search_actions.js';
import { getBrandsRequest } from 'shared/brands/actions/get_actions.js';

const sorted = R.compose(R.reverse, R.sortBy(R.prop('id')), R.values);

class IndexPage extends Component {
  constructor(props) {
    super();

    this.state = {
      choosed_brand: null,
      choosed_model: null,
      city: null,
      category: null,
      text: '',
    };

    this.search = debounce(500, this._search.bind(this));
    this.loadMore = this._search.bind(this, false);

    this._handleSearchInputChange = this._handleSearchInputChange.bind(this);

    this._handleBrandChange = props.handleBrandChange.bind(this, this.search);
    this._handleModelChange = props.handleModelChange.bind(this, this.search);
    this._handleCategoryChange = props.handleSelectChange.bind(this, this.search, 'category');
    this._handleCityChange = props.handleSelectChange.bind(this, this.search, 'city');
  }

  componentWillMount() {
    const { location } = this.props;

    this.setState({ category: location.query.category });
  }

  componentDidMount() {
    const { dispatch, advert_state: { search_adverts } } = this.props;

    if (sorted(search_adverts).length === 0) {
      this._search();
    }

    dispatch(getBrandsRequest());
  }

  _handleSearchInputChange(event) {
    this.setState({ text: event.target.value }, this.search);
  }

  _search(reset = true) {
    const { dispatch, advert_state: { meta } } = this.props;

    dispatch(searchAdvertsRequest({
      text: this.state.text,
      brand_id: this.state.choosed_brand ? this.state.choosed_brand.value : null,
      model_id: this.state.choosed_model ? this.state.choosed_model.value : null,
      city: this.state.city,
      category: this.state.category,
      page: reset ? null : meta.next_page,
    }, reset));
  }

  render() {
    const {
      advert_state: { search_adverts, is_searching, meta },
      current_user,
      brands,
      select_for_options,
      select_for_categories,
      select_for_cities,
    } = this.props;
    const { choosed_brand, choosed_model, city } = this.state;

    return (
      <block width="<960px">
        <Helmet
          title="объявления"
          meta={[
            { property: 'og:url', content: 'https://lacus.kz/obvs/search' },
            { property: 'og:type', content: 'obv' },
            { property: 'og:title', content: 'Lacus.kz - объявления' },
          ]}
        />
        <card className="tabListHorizontal -flex-row" cellPadding="16px" data-margin="8px">
          <Link to="/obvs" activeClassName="-active">Поиск</Link>
          {
            current_user.id ?
              <Link to="/obvs/own" activeClassName="-active">Мои объявления</Link>
            : null
          }
        </card>
        <column className="-flex-nowrap" display="sm:row">
          <column width="sm:240px" cellSpacing="8px">
            <card cellSpacing="8px" data-padding="8px">
              <row className="-content-justify">
                <div className="h6 line">Категории</div>
              </row>
              <column>
                <mg-input>
                  <Select
                    options={select_for_categories(CATEGORIES_NAMES)}
                    placeholder="Замена масел, жидкостей"
                    onChange={this._handleCategoryChange} value={this.state.category}
                  />
                </mg-input>
              </column>
            </card>
            <card cellSpacing="4px" data-padding="12px">
              <div className="h6">Поиск по авто</div>
              <mg-input>
                <Select
                  options={select_for_options(brands)}
                  placeholder="Honda"
                  onChange={this._handleBrandChange} value={choosed_brand}
                />
              </mg-input>
              {
                choosed_brand ?
                  <mg-input>
                    <Select
                      options={select_for_options(brands[choosed_brand.value].models)}
                      placeholder="Civic"
                      onChange={this._handleModelChange} value={choosed_model}
                    />
                  </mg-input>
                : null
              }
            </card>
            <card cellSpacing="4px" data-padding="12px">
              <div className="h6">Поиск по городу</div>
              {
                meta.cities ?
                  <mg-input>
                    <Select
                      options={select_for_cities(meta.cities)}
                      placeholder="Астана"
                      onChange={this._handleCityChange} value={city}
                    />
                  </mg-input>
                : null
              }
            </card>
            <card className="-items-center -text-center" cellSpacing="8px" data-padding="16px">
              <icon className="icon-36px icon-dark icon-information-outline" />
              <small>
                Объявления в этом разделе предоставлены пользователями.
                Вы сможете разместить объявление на любую необходимую тематику легко и абсолютно бесплатно
              </small>
              <Ability must="filled">
                <Link to="/obvs/new" className="button -text-primary">Добавить объявление</Link>
              </Ability>
            </card>
          </column>
          <column className="-flex-grow" cellSpacing="8px">
            <card className="-flex-row -flex-nowrap -items-center" cellSpacing="4px">
              <input
                className="inputMinimal -flex-grow"
                placeholder="Поиск"
                data-padding="12px"
                value={this.state.text}
                onChange={this._handleSearchInputChange}
              />
              <button shape="circle" data-padding="12px" color="blue-gray">
                <icon className="icon-magnify" />
              </button>
            </card>
            <card>
              <row className="h5 line -content-justify" cellSpacing="4px" data-padding="12px">
                <div>Список объявлений</div>
                <Ability must="filled">
                  <LinkButton to="/obvs/new" className="link">Добавить</LinkButton>
                </Ability>
              </row>
              <column className="itemList" height=">80px" cellPadding="8px">
                {
                  sorted(search_adverts).length > 0 ? null :
                    <column className="-items-center -text-center" cellSpacing="8px" data-padding="32px">
                      <div>Нет объявлений соответствующих выбранным критериям</div>
                    </column>
                }
                {
                  sorted(search_adverts).map(advert =>
                    <Link
                      to={{
                        pathname: `/obvs/${advert.slug}`,
                        state: { modal: true, title: advert.title },
                      }}
                      key={advert.id}
                      display="row"
                      className="-items-center -flex-nowrap"
                      cellSpacing="8px"
                    >
                      <photo
                        size="48"
                        placeholder="auto"
                        style={
                          advert.cover_thumb_url ? { backgroundImage: `url(${advert.cover_thumb_url})` } : null
                        }
                      />
                      <column>
                        <div className="-ellipsis -text-primary">{advert.title}</div>
                        <div className="-ellipsis -text-secondary">{advert.description}</div>
                      </column>
                    </Link>
                  )
                }
                {
                  sorted(search_adverts).length > 0 && meta.next_page ?
                    <Waypoint bottomOffset="-10%" onEnter={this.loadMore} />
                  : null
                }
              </column>
            </card>
            {
              is_searching ?
                <column height=">80px">
                  <Loader />
                </column>
              : null
            }
          </column>
        </column>
      </block>
    );
  }
}

IndexPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,

  advert_state: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
  brands: PropTypes.object.isRequired,
};

function select(state) {
  return {
    advert_state: state.client.advert_state,
    current_user: state.auth_state.current_user,
    brands: state.shared.brand_state.brands,
  };
}

const enhance = R.compose(
  connect(select),
  BaseHoc
);

export default enhance(IndexPage);
