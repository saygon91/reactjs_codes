import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Textarea from 'react-autosize-textarea';
import Dropzone from 'react-dropzone';

import BaseHoc from 'client/shared/base_hoc.jsx';
import DetailsForm from 'client/adverts/components/_details_form.jsx';

import Loader from 'shared/components/spinner.jsx';
import Select from 'shared/components/select.jsx';
import LinkButton from 'shared/components/link_button.jsx';
import PhonesInput from 'shared/components/phones_input.jsx';
import Errors from 'shared/components/errors.jsx';

import { getAdvertRequest } from 'client/adverts/actions/get_actions.js';
import { updateAdvertsRequest } from 'client/adverts/actions/update_actions.js';
import { getBrandsRequest } from 'shared/brands/actions/get_actions.js';

import { CATEGORIES_NAMES } from 'client/adverts/constants.js';

const clean = R.filter((x) => !R.isNil(x) && !R.isEmpty(x));

class EditPage extends Component {
  constructor(props) {
    super();

    this.state = {
      title: '',
      description: '',
      price: '',
      city: '',
      choosed_brand: null,
      choosed_model: null,
      category: '',
      phones: [],
      details: {},
    };

    this._init = this._init.bind(this);
    this._handlePhonesChange = this._handlePhonesChange.bind(this);
    this._updateAdverts = this._updateAdverts.bind(this);
    this._handleDetailsChange = this._handleDetailsChange.bind(this);

    this._onDrop = props.onDrop.bind(this, false);
    this._handleBrandChange = props.handleBrandChange.bind(this, false);
    this._handleModelChange = props.handleModelChange.bind(this, false);
    this._handleInputChange = props.handleInputChange.bind(this, false);
    this._handleCategoryChange = props.handleSelectChange.bind(this, false, 'category');
  }

  componentDidMount() {
    const { dispatch, params } = this.props;

    dispatch(getAdvertRequest(params.id));
    dispatch(getBrandsRequest());

    this._init(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this._init(nextProps);
  }

  _handleDetailsChange(details) {
    this.setState({ details });
  }

  _init(props) {
    const { advert_state: { adverts }, params: { id }, brands } = props;
    const advert = adverts[id];

    if (advert && !this.state.id) {
      const _state = clean(advert);

      if (advert.brand_id && !R.isEmpty(brands)) {
        const brand = brands[advert.brand_id];
        const model = R.find(R.propEq('id', advert.model_id))(brand.models);

        _state.choosed_brand = { value: brand.id, label: brand.title };
        _state.choosed_model = { value: model.id, label: model.title };
      }

      this.setState(_state);
    }
  }

  _handlePhonesChange(phones) {
    this.setState({ phones });
  }

  _updateAdverts() {
    const { dispatch, params } = this.props;
    const { choosed_model, choosed_brand } = this.state;

    const advert_params = {
      id: params.id,
      data: this.state,
    };

    advert_params.data.brand_id = choosed_brand ? choosed_brand.value : null;
    advert_params.data.model_id = choosed_model ? choosed_model.value : null;

    dispatch(updateAdvertsRequest(advert_params));
  }

  render() {
    const {
      advert_state: {
        errors,
        is_processing,
        is_saving,
      },
      brands,
      select_for_options,
      select_for_categories,
    } = this.props;
    const { choosed_brand, choosed_model } = this.state;

    if (is_processing) {
      return (
        <middle>
          <content data-padding="16px">
            <block className="-text-center">
              <header>
                <Loader color="#333" size="6px" margin="4px" />
              </header>
            </block>
          </content>
        </middle>
      );
    }

    return (
      <block width="<800px" cellSpacing="8px">
        <Errors errors={errors} />
        <card className="companyAdvertCard" data-padding="12px">
          <row>
            <column className="-flex-grow" cellSpacing="8px">
              <column cellSpacing="4px">
                <div className="inputMaterial h3">
                  <input
                    type="text"
                    onChange={this._handleInputChange}
                    name="title"
                    value={this.state.title}
                    required
                  />
                  <label htmlFor="title">название продукта <span>*</span></label>
                </div>
                <div className="inputMaterial">
                  <Textarea
                    onChange={this._handleInputChange}
                    name="description"
                    required
                    value={this.state.description}
                  />
                  <label htmlFor="description">описание продукта <span>*</span></label>
                </div>
              </column>
              <column>
                <div className="inputMaterial -w-1" data-margin="4px">
                  <input
                    type="number"
                    onChange={this._handleInputChange}
                    name="price"
                    value={this.state.price}
                    required
                    min="0" step="1"
                  />
                  <label htmlFor="price">стоимость</label>
                </div>
              </column>
              <column>
                <div className="inputMaterial -w-1" data-margin="4px">
                  <input
                    type="text"
                    onChange={this._handleInputChange}
                    name="city"
                    value={this.state.city}
                    required
                  />
                  <label htmlFor="city">город</label>
                </div>
              </column>
              <column className="-xs-12 -sm-6 -md-4">
                <label htmlFor="brands">Марка</label>
                <mg-input type="select">
                  <Select
                    options={select_for_options(brands)}
                    placeholder="Honda"
                    onChange={this._handleBrandChange}
                    value={choosed_brand}
                  />
                </mg-input>
              </column>
              {
                choosed_brand ?
                  <column className="-xs-12 -sm-6 -md-4">
                    <label htmlFor="models">Модель</label>
                    <mg-input type="select">
                      <Select
                        options={select_for_options(brands[choosed_brand.value].models)}
                        placeholder="Civic"
                        onChange={this._handleModelChange}
                        value={choosed_model}
                      />
                    </mg-input>
                  </column>
                : null
              }
              <column>
                <div className="inputMaterial -w-1" data-margin="4px">
                  <mg-input type="select">
                    <Select
                      options={select_for_categories(CATEGORIES_NAMES)}
                      placeholder=""
                      onChange={this._handleCategoryChange}
                      value={this.state.category}
                    />
                  </mg-input>
                </div>
              </column>

              {
                this.state.category ?
                  <DetailsForm
                    details={this.state.details}
                    category={this.state.category}
                    onChange={this._handleDetailsChange}
                  />
                : null
              }

              <PhonesInput phones={this.state.phones} onChange={this._handlePhonesChange} />

            </column>
            <div className="-xs-12 -sm-3 -flex-noshrink" data-padding="12px">
              <Dropzone className="dropzone" onDrop={this._onDrop} accept="image/*">
                {
                  this.state.cover || this.state.cover_thumb_url ?
                    <photo
                      className="advertPhoto -square"
                      style={{ backgroundImage: `url(${this.state.cover || this.state.cover_thumb_url})` }}
                    >
                      <layer className="-content-center -items-center -smooth">
                        <icon className="icon-48px icon-light icon-plus" />
                      </layer>
                    </photo>
                  :
                    <placeholder className="-square">
                      <layer className="-content-center -items-center">
                        <icon className="icon-48px icon-dark icon-camera" />
                      </layer>
                    </placeholder>
                }
              </Dropzone>
            </div>
          </row>
          <column cellSpacing="8px">
            <row cellSpacing="4px">
              <button onClick={this._updateAdverts} className="button -primary" disabled={is_saving}>
                {
                  is_saving ? 'Сохраняется...' : 'Сохранить'
                }
              </button>
              <LinkButton goBack className="button">Отмена</LinkButton>
            </row>
            {
              is_saving ? <Loader color="#333" size="6px" margin="4px" /> : null
            }
          </column>
        </card>
      </block>
    );
  }
}

EditPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  advert_state: PropTypes.object.isRequired,
  brands: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
};

function select(state) {
  return {
    advert_state: state.client.advert_state,
    brands: state.shared.brand_state.brands,
  };
}

const enhance = R.compose(
  connect(select),
  BaseHoc
);

export default enhance(EditPage);
