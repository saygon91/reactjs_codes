import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Waypoint from 'react-waypoint';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

import Loader from 'shared/components/spinner.jsx';
import LinkButton from 'shared/components/link_button.jsx';
import Ability from 'shared/components/ability.jsx';

import { getAdvertsRequest } from 'client/adverts/actions/get_actions.js';

const sorted = R.compose(R.reverse, R.sortBy(R.prop('id')), R.values);

class IndexPage extends Component {
  constructor() {
    super();

    this._load = this._load.bind(this);
  }

  componentDidMount() {
    this._load(true);
  }

  _load(reset = false) {
    const { dispatch, advert_state: { meta }, current_user } = this.props;

    const advert_params = {
      id: current_user.id,
      data: { page: reset ? null : meta.next_page },
    };

    dispatch(getAdvertsRequest(advert_params));
  }

  render() {
    const { advert_state: { own_adverts, is_processing, meta }, current_user } = this.props;

    return (
      <column width="<960px" cellSpacing="8px">
        <Helmet
          title={`объявления ${current_user.last_name} ${current_user.first_name}`}
          meta={[
            { property: 'og:url', content: 'https://lacus.kz/obvs/own' },
            { property: 'og:type', content: 'obvs' },
            {
              property: 'og:title',
              content: `Lacus.kz - объявления ${current_user.last_name} ${current_user.first_name}`,
            },
          ]}
        />
        <card className="tabListHorizontal -flex-row" cellPadding="16px">
          <Link to="/obvs/search" activeClassName="-active">Поиск</Link>
          {
            current_user ?
              <Link to="/obvs/own" activeClassName="-active">Мои объявления</Link>
            : null
          }
        </card>
        <card>
          <row className="h5 line -content-justify" cellSpacing="4px" data-padding="12px">
            <div>Список объявлений</div>
            <Ability must="filled">
              <LinkButton to="/obvs/new" className="link">Добавить</LinkButton>
            </Ability>
          </row>
          <column className="itemList" cellPadding="8px">
            {
              sorted(own_adverts).length > 0 ? null :
                <column className="-items-center -text-center" cellSpacing="8px" data-padding="32px">
                  <div>Нет объявлений</div>
                </column>
            }
            {
              sorted(own_adverts).map(advert =>
                <Link
                  to={`/obvs/${advert.slug}`}
                  key={advert.id}
                  className="-items-center -flex-nowrap"
                  display="row"
                  cellSpacing="8px"
                >
                  <photo
                    size="48"
                    placeholder="auto"
                    style={
                      advert.cover_thumb_url ? { backgroundImage: `url(${advert.cover_thumb_url})` } : null
                    }
                  />
                  <column>
                    <div className="-ellipsis -text-primary">{advert.title}</div>
                    <div className="-ellipsis -text-secondary">{advert.description}</div>
                  </column>
                </Link>
              )
            }
            {
              sorted(own_adverts).length > 0 && meta.next_page ?
                <Waypoint bottomOffset="-10%" onEnter={this.load} />
              : null
            }
          </column>
          {
            is_processing ? <Loader color="#333" size="6px" margin="4px" /> : null
          }
        </card>
      </column>
    );
  }
}

IndexPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  advert_state: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
};

function select(state) {
  return {
    advert_state: state.client.advert_state,
    current_user: state.auth_state.current_user,
  };
}

export default connect(select)(IndexPage);
