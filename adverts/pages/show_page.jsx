import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Link } from 'react-router';

import IndexComments from 'client/comments/pages/index.jsx';
import ImageGallery from 'company/shared/image_gallery.jsx';

import Loader from 'shared/components/spinner.jsx';
import SecretData from 'shared/components/secret_data.jsx';
import LinkButton from 'shared/components/link_button.jsx';
import PriceTag from 'shared/components/price_tag.jsx';

import { getAdvertRequest } from 'client/adverts/actions/get_actions.js';
import { deleteAdvertsRequest } from 'client/adverts/actions/delete_actions.js';

import { CATEGORIES_NAMES } from 'client/adverts/constants.js';

class ShowPage extends Component {
  constructor() {
    super();

    this._deleteAdverts = this._deleteAdverts.bind(this);
  }

  componentDidMount() {
    const { dispatch, params } = this.props;

    dispatch(getAdvertRequest(params.slug));
  }

  _deleteAdverts() {
    const { dispatch, advert_state: { current_advert_id } } = this.props;

    dispatch(deleteAdvertsRequest(current_advert_id));
  }

  render() {
    const {
      advert_state: {
        adverts,
        is_processing,
        current_advert_id,
      },
      current_user,
    } = this.props;

    const advert = adverts[current_advert_id];

    if (!advert) {
      return null;
    }

    return (
      <div className="modalCardBody -newScrollbar -flex-shrink" width="100% md:800px">
        {
          is_processing ?
            <div data-padding="16px">
              <column className="-text-center" width=">80px" height=">80px">
                <Loader />
              </column>
            </div>
          :
            <column data-padding="16px">
              <Helmet
                title={advert.title}
                meta={[
                  { property: 'og:url', content: `https://lacus.kz/obvs/${advert.slug}` },
                  { property: 'og:type', content: 'obv' },
                  { property: 'og:title', content: advert.title },
                  { property: 'og:description', content: advert.description },
                  { property: 'og:image', content: advert.cover_thumb_url },
                ]}
              />
              <row className="-sm-nowrap">
                <block width="100% sm:auto" cellSpacing="8px">
                  <card width="auto sm:240px" data-padding="8px" cellSpacing="8px">
                    <div>
                      <photo
                        shape="1/1"
                        placeholder="photo-fixed"
                        style={advert.cover_thumb_url ?
                          { backgroundImage: `url(${advert.cover_thumb_url})`, backgroundSize: 'cover' } : null
                        }
                      />
                    </div>
                  </card>
                  {
                    advert.phones && advert.phones.length > 0 ?
                      <card width="auto sm:240px" data-padding="8px" cellSpacing="8px">
                        <div>
                          <div className="dlTable">
                            <dl>
                              <dt width="80px">Телефоны</dt>
                              <dd data-padding="4px">
                                <SecretData label="Показать номера">
                                  {
                                    advert.phones.map((phone, key) =>
                                      <line key={key}>{phone}<br /></line>
                                    )
                                  }
                                </SecretData>
                              </dd>
                            </dl>
                          </div>
                        </div>
                      </card>
                    : null
                  }
                  {
                    advert.is_public ?
                      <card width="auto sm:240px" data-padding="8px" cellSpacing="8px">
                        <Link to={`/users/${advert.user.id}`} className="-items-center" cellSpacing="8px">
                          <avatar
                            size="40"
                            placeholder="user"
                            style={advert.user.avatar_thumb_url ? {
                              backgroundImage: `url(${advert.user.avatar_thumb_url})`,
                              backgroundSize: 'cover',
                            } : null}
                          />
                          <column>
                            <div className="lastName">{advert.user.last_name}</div>
                            <div className="firstName">{advert.user.first_name}</div>
                          </column>
                        </Link>
                      </card>
                    : null
                  }
                </block>
                <column className="-full" cellSpacing="8px">
                  <card data-padding="12px">
                    <column cellSpacing="4px" data-padding="8px">
                      <div className="h3 -ellipsis">{advert.title}</div>
                      {
                        advert.price ?
                          <div className="h2"><PriceTag price={advert.price} /></div>
                        : null
                      }
                    </column>
                    <row cellPadding="12px" data-children-width="50% md:33%">
                      <dl className="-flex-nogrow">
                        <dt>Категория</dt>
                        <dd>{CATEGORIES_NAMES[advert.category]}</dd>
                      </dl>
                      {
                        advert.city ?
                          <dl>
                            <dt>город</dt>
                            <dd>{advert.city} </dd>
                          </dl>
                        : null
                      }
                      {
                        R.keys(advert.details).map(key =>
                          <dl key={key} className="-flex-nogrow">
                            <dt>{key}</dt>
                            <dd>{advert.details[key]}</dd>
                          </dl>
                        )
                      }
                    </row>
                  </card>
                  <card data-padding="12px" cellSpacing="8px">
                    <column cellSpacing="4px">
                      <div className="h6">Описание</div>
                      <div dangerouslySetInnerHTML={{ __html: advert.description_html }} />
                      {
                        advert.description_params ?
                          <div>{advert.description_params}</div>
                        : null
                      }
                    </column>
                    {
                      advert.user_id === current_user.id ?
                        <row cellSpacing="4px">
                          <LinkButton
                            key="edit_advert_btn"
                            to={`/obvs/${advert.id}/edit`}
                            className="button -primary"
                          >
                            Редактировать
                          </LinkButton>
                          <button key="delete_advert_btn" onClick={this._deleteAdverts} className="button">
                            Удалить
                          </button>
                        </row>
                      : null
                    }
                  </card>
                  {
                    advert.pictures && advert.pictures.length > 0 ?
                      <card data-padding="8px">
                        <column data-padding="8px">
                          <div className="h6" data-padding="8px">Фото</div>
                          <ImageGallery images={advert.pictures} onlyShow />
                        </column>
                      </card>
                    : null
                  }
                  {
                    advert.is_commentable ?
                      <IndexComments commentable_klass="obvs" commentable_id={advert.id} />
                    : null
                  }
                </column>
              </row>
            </column>
        }
      </div>
    );
  }
}

ShowPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  advert_state: PropTypes.object.isRequired,
  current_user: PropTypes.object.isRequired,
};

function select(state) {
  return {
    advert_state: state.client.advert_state,
    current_user: state.auth_state.current_user,
  };
}

export default connect(select)(ShowPage);
