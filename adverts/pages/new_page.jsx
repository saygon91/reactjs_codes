import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Textarea from 'react-autosize-textarea';

import BaseHoc from 'client/shared/base_hoc.jsx';
import DetailsForm from 'client/adverts/components/_details_form.jsx';

import CoverSelect from 'shared/pictures/components/cover_select.jsx';
import Loader from 'shared/components/spinner.jsx';
import Select from 'shared/components/select.jsx';
import LinkButton from 'shared/components/link_button.jsx';
import PhonesInput from 'shared/components/phones_input.jsx';
import Errors from 'shared/components/errors.jsx';

import { getBrandsRequest } from 'shared/brands/actions/get_actions.js';
import { createAdvertsRequest } from 'client/adverts/actions/create_actions.js';

import { CATEGORIES_NAMES } from 'client/adverts/constants.js';

class NewPage extends Component {
  constructor(props) {
    super();

    this.state = {
      cover: '',
      category: null,
      phones: [],
      choosed_brand: null,
      choosed_model: null,
      details: {},
    };

    this._handlePhonesChange = this._handlePhonesChange.bind(this);
    this._createAdverts = this._createAdverts.bind(this);
    this._handleDetailsChange = this._handleDetailsChange.bind(this);

    this._handleBrandChange = props.handleBrandChange.bind(this, false);
    this._handleModelChange = props.handleModelChange.bind(this, false);
    this._handleCategoryChange = props.handleSelectChange.bind(this, false, 'category');
    this._handleCoverChange = props.handleCoverChange.bind(this, false);
  }

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(getBrandsRequest());
  }

  _handleDetailsChange(details) {
    this.setState({ details });
  }

  _handlePhonesChange(phones) {
    this.setState({ phones });
  }

  _createAdverts() {
    const { dispatch } = this.props;
    const { choosed_model, choosed_brand } = this.state;

    const advert_params = {
      title: this.title.value,
      description: this.description.textarea.value,
      price: this.price.value,
      city: this.city.value,
      cover: this.state.cover,
      brand_id: choosed_brand ? choosed_brand.value : null,
      model_id: choosed_model ? choosed_model.value : null,
      category: this.state.category,
      phones: this.state.phones,
      details: this.state.details,
    };

    dispatch(createAdvertsRequest(advert_params));
  }

  render() {
    const { brands, is_processing, errors, select_for_options, select_for_categories } = this.props;
    const { choosed_model, choosed_brand } = this.state;

    return (
      <middle width="100% md:auto">
        <content>
          <card className="redesign" width="md:640px">
            <row className="-split" data-padding="32px">
              <div className="h3 line">Добавить объявление</div>
            </row>
            <column className="formErrors">
              <Errors errors={errors} />
            </column>
            <column className="-split" data-padding="20px">
              <column display="sm:row" className="-flex-nowrap" cellSpacing="4px">
                <div width="sm:160px" data-padding="8px">
                  <CoverSelect
                    onSelect={this._handleCoverChange}
                    value={this.state.cover}
                  />
                </div>
                <column className="-flex-grow -content-end">
                  <column cellSpacing="4px" data-margin="4px">
                    <label htmlFor="title">Название <span color="red">*</span></label>
                    <mg-input>
                      <input
                        className="input"
                        name="title"
                        ref={(c) => { this.title = c; }}
                        type="text"
                        placeholder="Заголовок объявления"
                      />
                    </mg-input>
                  </column>
                  <row className="-flex-nowrap" cellSpacing="4px">
                    <column width="50%" cellSpacing="4px">
                      <label htmlFor="description">Категория <span color="red">*</span></label>
                      <mg-input>
                        <Select
                          options={select_for_categories(CATEGORIES_NAMES)}
                          placeholder="Замена масел, жидкостей"
                          onChange={this._handleCategoryChange}
                          value={this.state.category}
                        />
                      </mg-input>
                    </column>
                    <column width="50%" cellSpacing="4px">
                      <label htmlFor="city">Город</label>
                      <mg-input>
                        <input
                          className="input"
                          name="city"
                          ref={(c) => { this.city = c; }}
                          type="text"
                          placeholder="Астана"
                        />
                      </mg-input>
                    </column>
                  </row>
                </column>
              </column>
              <column cellSpacing="4px" data-padding="8px">
                <label htmlFor="description">Описание <span color="red">*</span></label>
                <mg-input>
                  <Textarea
                    rows="3"
                    className="textarea"
                    name="description"
                    ref={(c) => { this.description = c; }}
                    placeholder="Описание объявления"
                  />
                </mg-input>
              </column>
            </column>
            <column className="-split" data-padding="24px">
              <column
                display="sm:row"
                className="-flex-nowrap"
                cellPadding="4px"
                data-children-width="sm:1/3"
              >
                <column cellSpacing="4px">
                  <label htmlFor="price">Цена</label>
                  <mg-input>
                    <input
                      className="input"
                      name="price"
                      ref={(c) => { this.price = c; }}
                      type="number"
                      placeholder="Стоимость"
                    />
                  </mg-input>
                </column>
                <column cellSpacing="4px">
                  <label htmlFor="brands">Марка</label>
                  <mg-input>
                    <Select
                      options={select_for_options(brands)}
                      placeholder="Honda"
                      onChange={this._handleBrandChange}
                      value={choosed_brand}
                    />
                  </mg-input>
                </column>
                {
                  choosed_brand ?
                    <column cellSpacing="4px">
                      <label htmlFor="models">Модель</label>
                      <mg-input>
                        <Select
                          options={select_for_options(brands[choosed_brand.value].models)}
                          placeholder="Civic"
                          onChange={this._handleModelChange}
                          value={choosed_model}
                        />
                      </mg-input>
                    </column>
                  : null
                }
              </column>
              {
                this.state.category ?
                  <DetailsForm category={this.state.category} onChange={this._handleDetailsChange} />
                : null
              }
              <column data-padding="4px">
                <PhonesInput phones={this.state.phones} onChange={this._handlePhonesChange} />
              </column>
            </column>
            <row cellSpacing="4px" data-padding="28px">
              <button onClick={this._createAdverts} className="button -primary" disabled={is_processing} >
                {
                  is_processing ? 'Сохраняется...' : 'Сохранить'
                }
              </button>
              <LinkButton goBack className="button">Отмена</LinkButton>
            </row>
            {
              is_processing ? <Loader color="#333" size="6px" margin="4px" /> : null
            }
          </card>
        </content>
      </middle>
    );
  }
}

NewPage.propTypes = {
  brands: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  is_processing: PropTypes.bool.isRequired,
  errors: PropTypes.array.isRequired,
};

function select(state) {
  return {
    brands: state.shared.brand_state.brands,
    is_processing: state.client.advert_state.is_processing,
    errors: state.client.advert_state.errors,
  };
}

const enhance = R.compose(
  connect(select),
  BaseHoc
);

export default enhance(NewPage);
